# A simple "search your contacts" application

### How to run the app
Open a terminal and:
1. Clone the repo `git clone https://bitbucket.org/anarchos78/search-your-contacts-app.git`  
2. Go into the application directory `cd search-your-contacts-app`  
3. Create a virtual environment for the project, using python 3 (3.6.1 preferably) and activate it (I prefer ***pyenv***)  
4. Run `pip install -r requirements.txt` to install the required packages for the app  
5. Run the server: `python run.py`  
6. Open a browser and go to http://127.0.0.1:5000/  

**Enjoy!**
