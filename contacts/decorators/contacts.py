# -*- coding: utf-8 -*-

"""
.. module:: .contacts_json.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

import os

from pathlib import Path
from functools import wraps

import ijson

from flask import render_template


def contacts_all(func):
    """
    Takes as input a route and returns a response object
    :param func: Route
    :return: A response object
    """

    @wraps(func)
    def decorated_function(*args, **kwargs):
        """Inner function"""
        current_dir = os.path.dirname(__file__)

        f = open(
            os.path.join(
                Path(current_dir).parent,
                'data/exercise-data.json'
            )
        )

        items = [
            {
                k: ', '.join(v)
                if isinstance(v, list) and v[0]
                else 'N/A' if isinstance(v, list) and not v[0]
                else v
                for k, v in contact.items()
            }
            for contact in ijson.items(f, 'item')
        ]

        return render_template('index.html', contacts=items, count=len(items))

    return decorated_function
