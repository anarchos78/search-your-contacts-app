# -*- coding: utf-8 -*-

"""
.. module:: .contacts_filter.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

import os

from pathlib import Path
from functools import wraps

import ijson

from flask import render_template, request


def contacts_filter(func):
    """
    Takes as input a route and returns a response object
    :param func: Route
    :return: A response object
    """

    @wraps(func)
    def decorated_function(*args, **kwargs):
        """Inner function"""

        """
        Keep in mind that the empty string is present in all strings.
        With that in mind, filtering the result set with empty string will
        return the whole data set
        """

        search_term = request.form['searchTerm']

        current_dir = os.path.dirname(__file__)

        f = open(
            os.path.join(
                Path(current_dir).parent,
                'data/exercise-data.json'
            )
        )

        items_flatten = [
            {
                k: ', '.join(v)
                if isinstance(v, list) and v[0]
                else 'N/A' if isinstance(v, list) and not v[0]
                else v
                for k, v in contact.items()
            }
            for contact in ijson.items(f, 'item')
        ]

        result = [
            item for item in items_flatten
            if
            {
                v for v in {k: v.lower() for k, v in item.items()}.values()
                if search_term.lower() in v
            }
        ]

        return render_template(
            'index.html',
            searchterm=search_term,
            contacts=result,
            count=len(result)
        )

    return decorated_function
