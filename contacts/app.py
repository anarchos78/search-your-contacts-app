# -*- coding: utf-8 -*-

"""
.. module:: .contacts.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

from flask import Flask

from config.config import configure_app
from contacts.utils.filters import pluralize


def create_app():
    app = Flask(__name__)
    configure_app(app)

    # Add your custom filter
    app.jinja_env.filters['pluralize'] = pluralize

    return app
