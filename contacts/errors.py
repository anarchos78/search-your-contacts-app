# -*- coding: utf-8 -*-

"""
.. module:: .errors.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

from flask import render_template

from contacts.views import contacts


@contacts.errorhandler(403)
def forbidden(e):
    return render_template('403.html', error=str(e))


@contacts.errorhandler(404)
def page_not_found(e):
    return render_template('404.html', error=str(e))


@contacts.errorhandler(405)
def method_not_allowed(e):
    return render_template('405.html', error=str(e))


@contacts.errorhandler(410)
def gone(e):
    return render_template('410.html', error=str(e))


@contacts.errorhandler(500)
def server_error(e):
    return render_template('500.html', error=str(e))
