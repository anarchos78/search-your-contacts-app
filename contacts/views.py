# -*- coding: utf-8 -*-

"""
.. module:: .views.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

from flask import Blueprint, request

from contacts.decorators.contacts import contacts_all
from contacts.decorators.contacts_filter import contacts_filter

contacts = Blueprint('contacts', __name__,
                     template_folder='templates',
                     static_folder='static',
                     static_url_path='/contacts/static')


@contacts.route('/')
@contacts_all
def index():
    """
    The starting point
    """
    return request.args


@contacts.route('/results', methods=['POST'])
@contacts_filter
def results():
    """
    Search endpoint
    """
    return request.args
