# -*- coding: utf-8 -*-

"""
.. module:: .filters.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""


def pluralize(number, singular='', plural='s'):
    """
    Custom filter to pluralize a word based on record count [first parameter]
    :param number: int
    :param singular: string
    :param plural: string
    :return: string
    """
    if number == 1:
        return singular
    else:
        return plural
