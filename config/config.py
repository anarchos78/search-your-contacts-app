# -*- coding: utf-8 -*-

"""
.. module:: .config.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

import logging

from contacts.views import contacts
from contacts.errors import (
    forbidden, page_not_found, method_not_allowed, gone, server_error
)


class BaseConfig(object):
    """
    Base configuration class.
    Here you can define common configuration
    that other config classes will inherit from
    """
    DEBUG = False
    TESTING = False
    LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    LOGGING_LOCATION = 'my-awesome-app.log'
    LOGGING_LEVEL = logging.WARNING


class DevConfig(BaseConfig):
    """
    Development configuration class
    """
    DEBUG = True
    TESTING = True


class TestConfig(BaseConfig):
    """
    Test configuration class
    """
    DEBUG = False
    TESTING = True


class ProdConfig(BaseConfig):
    """
    Test Production class
    """
    DEBUG = False
    TESTING = False


def configure_app(app):
    """
    App configuration
    """
    app.config.from_object('config.config.ProdConfig')

    app.register_blueprint(contacts)

    # Register error pages
    app.register_error_handler(403, forbidden)
    app.register_error_handler(404, page_not_found)
    app.register_error_handler(405, method_not_allowed)
    app.register_error_handler(410, gone)
    app.register_error_handler(500, server_error)

    # Configure logging
    handler = logging.FileHandler(app.config['LOGGING_LOCATION'])
    handler.setLevel(app.config['LOGGING_LEVEL'])
    formatter = logging.Formatter(app.config['LOGGING_FORMAT'])
    handler.setFormatter(formatter)
    app.logger.addHandler(handler)
