# -*- coding: utf-8 -*-

"""
.. module:: .rolepoint-task.py.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

from contacts.app import create_app

if __name__ == '__main__':
    application = create_app()
    application.run()
